<!-- START MODAL CONFIRMATION -->
<div class="modal fade" id="confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <img class="sing-in-img-logo" src="" alt="">
            <img class="sing-in-img" src="../../resources/img/sign-in.svg" alt="">
            <div class="modal-header">
                <h5 class="modal-title modal-title-confirmation" id="exampleModalLabel">Confirma seu email!</h5>
                <button type="button" class="close close-button-sign-in" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <span class="span-confirmation"> Sua conta foi registrada com sucesso. Para concluir o processo,
                        verifique seu e-mail para uma solicitação de validação.</span>
            </div>
        </div>
    </div>
</div>

<!--END MODAL CONFIRMATION -->